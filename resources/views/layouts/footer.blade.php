<!-- jQuery -->
<script src="{{ asset('public/bower_components/jquery/dist/jquery.min.js') }}"></script>
<!-- Include all compiled bootstrap plugins -->
<script src="{{ asset('public/bower_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>
<!-- Datatables -->
<script src="{{ asset("public/bower_components/datatables/media/js/jquery.dataTables.min.js") }}"></script>
<script src="{{ asset("public/bower_components/datatables/media/js/dataTables.bootstrap.min.js") }}"></script>
<!-- Custom JS -->
<script src="{{ asset('public/js/custom.js') }}"></script>
</body>
</html>