@include('layouts.header')
@include('layouts.navigation')

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <br><br><br><br><br><br><br><br><br><br><br>
            <h1 class="text-center">Web ATM</h1>
            <div>
            ATM Balance: 
            <table class="table">
            <tr>
                <td>
                    {{$notes[0]['amount']}} x 20 &euro;
                </td>
            <tr>
            <tr>
                <td>
                    {{$notes[1]['amount']}} x 50 &euro;
                </td>
            </tr>

            <tr>
                <td>
                    Total: &euro; {{ ( $notes[1]['amount']*50 + $notes[0]['amount']*20 ) }}
                </td>
            </tr>

            </table>
            </div>
        </div>
    </div>
</div>

@include('layouts.footer')