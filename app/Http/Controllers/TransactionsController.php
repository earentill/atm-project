<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Account;
use Illuminate\Support\Facades\Auth;
use App\Models\TransactionType;
use App\Models\Transaction;
use App\Models\Note;
use App\Models\NoteType;
use Webpatser\Uuid\Uuid;

class TransactionsController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $transactions = Transaction::where('user_id', Auth::user()->id)->get();

        return view('transactions.index', [
            'transactions' => $transactions
        ]);
    }

    /**
     * Display welcome page.
     *
     * @return \Illuminate\Http\Response
     */
    public function welcome(){
        $notes = Note::all()->toArray();

        return view('welcome', ['notes' => $notes]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $accounts = Account::where('user_id', Auth::user()->id)
                ->orderBy('name', 'asc')
                ->get();

        $transaction_types = TransactionType::orderBy('name', 'asc')->get();

        return view('transactions.create', [
            'accounts' => $accounts,
            'transaction_types' => $transaction_types
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $this->validate($request, [
            'account' => 'required',
            'transaction_type' => 'required',
            'amount' => 'required|numeric',
        ]);


        $update = Account::where('id', $request->account)->first();
        
        
        if ($request->transaction_type === '1') {
            //TO-DO deposit 
            //$update->balance = $update->balance + $request->amount;
            return redirect('transactions');
        } elseif ($request->transaction_type === '3') {
            // A withdrawal. Check to ensure minimum balance does not go below zero
            if ($update->balance < $request->amount) {
                return redirect('transactions')
                                ->with('danger', $update->name . '\'s account balance (euro ' . number_format($update->balance) . ') is less than requested withdrawal amount (euro ' . number_format($request->amount) . ').');
            } else {
                //amount to notes and total 
                $atn = $this->amountToNotes($request->amount);
                //dd( $atn );
                if( $atn['total'] < $request->amount  ){
                    return redirect('transactions')
                                ->with('danger', $update->name . '\'s atm balance (euro ' . number_format($atn['total']) . ') is less than requested withdrawal amount (euro ' . number_format($request->amount) . ').');
                }
                
                if( $atn['20'] > 0 || $atn['50'] > 0){
                    $update->balance = $update->balance - $request->amount;
                    $twenties = Note::where(['note_id' => 1])->first();
                    $fifties = Note::where(['note_id' => 2])->first();

                    $twenties->amount = $twenties->amount - $atn['20'];
                    $twenties->save();

                    $fifties->amount = $fifties->amount - $atn['50'];
                    $fifties->save();

                }else{

                    if( $atn['20'] == 0 ){
                        return redirect('transactions')
                        ->with('danger', $update->name . '\'s Requested amount is not valid. Please choose amount that can be served by 20s bank notes');
                    }

                    if( $atn['50'] == 0 ){
                        return redirect('transactions')
                        ->with('danger', $update->name . '\'s Requested amount is not valid. Please choose amount that can be served by 50s bank notes');
                    }
                    
                }
                
                
            }
        }

        $update->save();

        $insert = new Transaction;
        $insert->uuid = Uuid::generate(4);
        $insert->user_id = Auth::user()->id;
        $insert->type_id = $request->transaction_type;
        $insert->account_id = $request->account;
        $insert->amount = $request->amount;
        $insert->save();

        return redirect('transactions')->with('success', 'Record Added');
    }

    public function amountIsValid($reqAmount){
        return ( $reqAmount %20 == 0 || $reqAmount %50 == 0 )?true:false;
    }

    public function amountToNotes($reqAmount){
        
        
        $notes['20'] = Note::where([
            ['note_id', '=',  1]
        ])
        ->select('amount')
        ->first()
        ->toArray()['amount'];

        $notes['50'] = Note::where([
            ['note_id', '=',  2]
        ])
        ->select('amount')
        ->first()
        ->toArray()['amount'];

        $total = ( $notes['50'] * 50 + $notes['20'] * 20  );
        $banknotes=[];
        $banknotes['total'] = $total;

        

        if( $notes['20'] >= (int) ( $reqAmount / 20 )  && $reqAmount %20 == 0 ){
            
                $banknotes['20'] = (int) ( $reqAmount / 20 ) ;
                $banknotes['50'] = 0 ;
            
        }elseif( $notes['50'] >= (int) ( $reqAmount / 50 ) && $reqAmount %50 == 0 ){
            
                $banknotes['20'] = 0 ;
                $banknotes['50'] = (int) ( $reqAmount / 50 ) ;
            
        }else if( $notes['20'] >= (int) ( $reqAmount / 20 ) &&  $notes['50'] >= (int) ( ( $reqAmount %20 ) / 50 ) && ( $reqAmount %20 )%50 == 0 ){
            
                $banknotes['20'] = (int) ( $reqAmount / 20 ) ;
                $banknotes['50'] = (int) ( ( $reqAmount %20 ) / 50 ) ;
            
        }elseif( $notes['20'] >= (int) ( ( $reqAmount %50 ) / 20 ) &&  $notes['50'] >= (int) ( $reqAmount / 50 ) && ( $reqAmount %50 )%20 == 0 ){
            
                $banknotes['20'] = (int) ( ( $reqAmount %50 ) / 20 ) ;
                $banknotes['50'] = (int) ( $reqAmount / 50 ) ;
            
        }else{
            
                $banknotes['20'] = 0 ;
                $banknotes['50'] = 0 ;
            
        }
        
        
        
        return $banknotes;
    }

}
