<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Note extends Model {

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'note_id', 'amount'
    ];

    /**
     * Get the note type.
     */
    public function Type() {
        return $this->belongsTo('App\Models\NoteType');
    }

}
