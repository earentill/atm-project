<?php

use Illuminate\Database\Seeder;

class TransactionTypesTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {

        /**
         * Transaction Types
         */
        $types = [
            'Deposit', 'Funds Transfer', 'Withdrawal'
        ];
        $insert = [];
        foreach ($types as $type) {
            $insert [] = [
                'name' => $type,
            ];
        }
        DB::table('transaction_types')->insert($insert);
        
        /**
         * Default User 
         */
        $user = [];
        $user = [
            'uuid'               => '6d784c2d-8f23-47ad-a1e9-779834933d2f',
            'first_name'        => 'John',
            'last_name'         => 'Doe',
            'email'             => 'john.doe.demo@exciteholidays.com',            
            'password'          => '$2y$10$b27jeiVZssJfc3s7F6GpJeKGgn/LKou7TXZ0KAZMy8JsuNqiCB10u',
            'remember_token'    => 'FLMVtTqy40euxE6NrZL3L9hGlgKaNJ5IQLxhfJu8bk27xHimlQqZ6iHbQwug'

        ];
        DB::table('users')->insert($user);

        /**
         * Account defautl bound to user
         */
        $account = [
            [ 
                'uuid' => 1,
                'user_id' => 1,
                'name' => 'Demo account',
                'balance' => 200,
                'created_at' => date('y-m-d H:i:s')
            ]
        ];
        DB::table('accounts')->insert($account);          

        /**
         * Notes types (20 euro note and 50 euro note)
         */
        $noteTypes = [
            [ 
                'note_title' => '20 euro',
                'price' => 20
            ],
            [ 
                'note_title' => '50 euro',
                'price' => 50
            ],
            

        ];
        DB::table('note_type')->insert($noteTypes);        

        /**
         * Notes available in ATM
         */
        $notes = [
            [ 
                'note_id' => 1,
                'amount' => 10
            ],
            [ 
                'note_id' => 2,
                'amount' => 10
            ],
        ];
        DB::table('notes')->insert($notes);  
        
        
    }

}
