<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNotesTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('note_type', function (Blueprint $table) {
            $table->increments('id');
            $table->string('note_title');
            $table->integer('price');
            $table->timestamps();
        });

        Schema::create('notes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('note_id')->unsigned();
            $table->integer('amount');
            $table->timestamps();

            $table->foreign('note_id')->references('id')->on('note_type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('note_types');
        Schema::dropIfExists('notes');
    }

}
